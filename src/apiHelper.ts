const API_URL ='https://api.themoviedb.org/3'
const API_KEY ='d5dbfb47783d6267d3e73a02de090078'

export async function callApi(endpoint: string, method: string,page:number,val?:string) {

    const url = val?`${API_URL}${endpoint}?api_key=${API_KEY}&query=${val}&page=${page}`:`${API_URL}${endpoint}?api_key=${API_KEY}&page=${page}`;
    const options = {
        method,
    };
    return await fetch(url, options)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            return data;
        });
}