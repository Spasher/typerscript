import { makeLike } from './eventHandler';
import { getFavs } from './localStorageHelper';
import { callApi } from './apiHelper';


export async  function randomFilm():Promise<void>{
    await callApi('/movie/popular','GET',1)
        .then((res)=>{
            takeRandom(res)
        })
}
function takeRandom(res:any){

    const item = res.results[Math.floor(Math.random()*res.results.length)];
    const root = document.getElementById('random-movie')
    if(root)root.innerHTML = ''
    const randEl = document.createElement('div')
    randEl.classList.add(...'row py-lg-5'.split(' ').filter(Boolean))
    randEl.style.backgroundImage = `url(https://image.tmdb.org/t/p/original/${item.backdrop_path})`

    const plot = document.createElement('div')
    plot.classList.add(...'col-lg-6 col-md-8 mx-auto'.split(' ').filter(Boolean))
    plot.style.backgroundColor= '#2525254f'

    const h1 = document.createElement('h1')
    h1.classList.add(...' fw-light text-light'.split(' ').filter(Boolean))
    h1.id = 'random-movie-name'
    h1.innerText = item.original_title

    const p = document.createElement('p')
    p.classList.add(...' lead text-white'.split(' ').filter(Boolean))
    p.id = 'random-movie-description'
    p.innerText = item.overview

    plot.appendChild(h1)
    plot.appendChild(p)

    randEl.appendChild(plot)

    root?.appendChild(randEl)
}

export function createPosts(res:any):void{
    for (const item of res.results) {
        createPost(item.poster_path,item.overview,item.release_date,item.id)
        createFavPost(item.poster_path,item.overview,item.release_date,item.id)
    }
}

function createFavPost(imageLink:string,description:string,date:string,id:string):void{
   const favList = getFavs()
   if(!favList.includes(`${id}`)){
      return
   }
   const favRoot = document.getElementById('favorite-movies')
   const  fav = document.createElement('div')
   fav.classList.add(...'col-12 p-2'.split(' ').filter(Boolean))
   fav.id = id

   const cardShadow = createElement(imageLink, description, date, id)

   fav.append(cardShadow)

   favRoot?.appendChild(fav)
}


function createPost(imageLink:string,description:string,date:string,id:string):void{

    const root = document.getElementById('film-container')
    const  post = document.createElement('div')
    post.classList.add(...'col-lg-3 col-md-4 col-12 p-2'.split(' ').filter(Boolean))
    post.id = id

    const cardShadow = createElement(imageLink,description,date,id)

    post.append(cardShadow)

    root?.appendChild(post)
}


function createElement(imageLink:string,description:string,date:string,id:string):HTMLElement{
   const favList = getFavs()
   const cardShadow = document.createElement('div')
   cardShadow.classList.add(...'card shadow-sm'.split(' ').filter(Boolean))

   const img = document.createElement('img')
   img.src = 'https://image.tmdb.org/t/p/original/' + imageLink

   const svg = document.createElementNS('http://www.w3.org/2000/svg','svg')
   svg.setAttributeNS(null,'stroke','red')
   svg.setAttributeNS(null,'fill',`${favList.includes(`${id}`)?'red':'#ff000078'}`)
   svg.setAttributeNS(null,'width','50')
   svg.setAttributeNS(null,'height','50')
   svg.setAttributeNS(null,'class','bi bi-heart-fill position-absolute p-2')
   svg.setAttributeNS(null,'viewBox','0 -2 18 22')
   svg.addEventListener('click',makeLike)
   svg.innerHTML = ' <path\n' +
       '                                fill-rule="evenodd"\n' +
       '                                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"\n' +
       '                            />'
   svg.style.cursor = 'pointer'

   const cardBody = document.createElement('div')
   cardBody.classList.add('card-body')

   const text = document.createElement('p')
   text.classList.add(...'card-text truncate'.split(' ').filter(Boolean))
   text.innerText = description

   const dateTime = document.createElement('div')
   dateTime.classList.add(...'d-flex justify-content-between align-items-center'.split(' ').filter(Boolean))
   dateTime.innerHTML = `<small class="text-muted">${date?date:'Soon!'}</small>`

   cardBody.appendChild(text)
   cardBody.appendChild(dateTime)

   cardShadow.appendChild(img)
   cardShadow.append(svg)
   cardShadow.appendChild(cardBody)

   return cardShadow
}