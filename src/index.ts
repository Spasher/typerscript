import { upcoming, popular, topRated, loadMore, search } from './eventHandler';
import { randomFilm } from './DomHelper';


export async function render(): Promise<void> {
    popular()
    randomFilm()

    document.getElementById('submit')?.addEventListener('click',search)

    document.getElementById('upcoming')?.addEventListener('click',upcoming)
    document.getElementById('top_rated')?.addEventListener('click',topRated)
    document.getElementById('popular')?.addEventListener('click',popular)

    document.getElementById('load-more')?.addEventListener('click',loadMore)



}
