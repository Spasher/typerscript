export function addFav(id:number):void{
    localStorage.setItem(`${id}`,'fav')
}
export function delFav(id:number):void{
    localStorage.removeItem(`${id}`)

}
export function getFavs():string[]{
    const favList:string[] = []
    for(let i=0; i<localStorage.length; i++) {
        const key = localStorage.key(i);
        if (typeof key === 'string') {
            favList.push(key);
        }
    }

    return favList
}