import { callApi } from './apiHelper';
import { createPosts } from './DomHelper';
import { addFav, delFav } from './localStorageHelper';


let active = 'popular'
let pageNum = 1
export  function upcoming():void{
    pageNum = 1
    active ='upcoming'
    makeContent('/movie/upcoming',pageNum)


}
export  function topRated():void{
    pageNum = 1
    active = 'top_rated'
    makeContent('/movie/top_rated',pageNum)
}
export  function popular():void{
    pageNum = 1
    active ='popular'

    makeContent('/movie/popular',pageNum)
}

export  function loadMore():void{
    pageNum += 1
    makeContent(`/movie/${active}`,pageNum)
}

export  function search(this: any):void{
    const val = this.previousElementSibling.value;
    makeContent('/search/movie',pageNum,val?val:'')


}

export function makeLike(this: any):void{
    const filled = this.getAttributeNS(null,'fill')
    const id = this.parentElement.parentElement.id
    if(filled=='#ff000078'){
        this.setAttributeNS(null,'fill','red')
        addFav(id)

    }else {

        this.setAttributeNS(null,'fill','#ff000078')
        delFav(id)
    }
    makeContent(`/movie/${active}`,pageNum)
}


async function makeContent(url:string,page:number,val?:string):Promise<void>{

     await callApi(url,'GET',page,val)
        .then((res)=>{
            if(page==1) {
                const root = document.getElementById('film-container')
                if (root) root.innerHTML = ''
            }
            const favRoot = document.getElementById('favorite-movies')
            if(favRoot)favRoot.innerHTML = ''
            createPosts(res)
        })
}